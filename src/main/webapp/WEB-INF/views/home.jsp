<%@ page session="false" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title>Home</title>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/jquery-1.11.0.js"></script>
	<script type="text/javascript">
		$(function(){
			alert($("#hrefTest").attr("href"));
		});
	</script>
</head>
<body>
<h1>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>
<a id="hrefTest" href="<c:url value="/doSomeThing"/>">TEST</a>
</body>
</html>
